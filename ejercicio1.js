function miFuncionDeComparar(numero1, numero2) {
    // Recuerda completar los condicionales 'if'

    if (numero1 === 5) {
        console.log("numero1 es estrictamente igual a 5");
    }

    if (numero1<numero2) {
        console.log("numero1 no es mayor que numero2");
    }
    if (numero2>=0) {
        console.log("numero2 es positivo");
    }
    if (numero1<0||numero1!=0) {
        console.log("numero1 es negativo o distinto de cero");
    }
}

// Prueba con distintos números, por ejemplo con 5 y 2
miFuncionDeComparar(5, 2);

// Esto deberías ver por pantalla:
// "numero1 es estrictamente igual a 5"
// "numero2 es positivo"